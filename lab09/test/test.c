/**
 * @file test.c
 * Test of function snfunction for lab09.
 * @author Vyacheslav
 * @version 0.0.1
 * @date 17.02
 */
#include <stdlib.h>
#include <check.h>
#include "../src/lib.h"
#include "../src/lib.c"

START_TEST(test_first_type_1)
{
    int number = 11;
    int expected = 0;
    int actual = first_type(number);
    ck_assert_int_eq(expected, actual);
}
END_TEST


Suite * main_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("lib");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_first_type_1);

    suite_add_tcase(s, tc_core);

    return s;
}
int main(void)
{
    return 0;
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = main_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
