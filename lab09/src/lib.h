/**
 * @file lib.h
 * Header file for lib.c for lab09 
 * @author Vyacheslav
 * @version 0.0.1
 * @date 17.02
 */
 
/**
 * Prints "1" if argument is a simple number.
 * @param number The number to check
 * @return 1 if simple, 0 if not
 */
int first_type();

